const path = require('path');
const express = require('express');
const log = require('./utils');
const hbs = require('hbs');
const app = express();
const geocode = require('../src/utils/geocode');
const forecast = require('../src/utils/forecast');

const port = process.env.PORT || 3000;

const publicDirectoryPath = path.join(__dirname, '../public');
const viewsPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');

app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);

app.use(express.static(publicDirectoryPath));

app.get('', (req, res) => {
    res.render('index', {
        title: 'Weather',
        name: 'Some'
    });
});

app.get('/weather', (req, res) => {
    if (!req.query.address) {
        return res.send({
            error: 'Location must be provide'
        });
    }

    geocode(
        req.query.address,
        (error, { latitude, longitude, location } = {}) => {
            if (error) {
                return res.send({
                    error
                });
            }
            forecast(latitude, longitude, (error, forecastData) => {
                if (error) {
                    return res.send({
                        error
                    });
                }
                res.send({
                    location,
                    address: req.query.address,
                    forecast: forecastData
                });
            });
        }
    );
});

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About'
    });
});

app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Help'
    });
});

app.get('/help/*', (req, res) => {
    res.render('error', {
        error: 'Help article not found'
    });
});

app.get('*', (req, res) => {
    res.render('error', {
        error: 'Page not found'
    });
});

app.listen(port, () => {
    console.log(`Server is up on port ${port}.`);
});
