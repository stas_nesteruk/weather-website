const request = require('request');
const ACCESS_TOKEN = '90ace7db87ee06feb247d4a2a97d6c11';
const forecast = (latitude, longitude, callback) => {
    const url = `https://api.darksky.net/forecast/${ACCESS_TOKEN}/${latitude},${longitude}?units=si`;
    request({ url, json: true }, (err, { body } = {}) => {
        if (err) {
            callback('Unable to connect to weather service', undefined);
        } else if (body.error) {
            callback('Unable to find location', undefined);
        } else {
            const msg =
                'It is currently %s degrees out. There is a %s% chance of rain.';
            const { temperature, precipProbability } = body.currently;
            const {
                summary,
                temperatureMin,
                temperatureMax
            } = body.daily.data[0];
            callback(
                undefined,
                `${summary} It is currently ${temperature} degrees out. There is a ${precipProbability}% chance of rain.
                This low today is ${temperatureMin}, with a high of ${temperatureMax}`
            );
        }
    });
};

module.exports = forecast;
