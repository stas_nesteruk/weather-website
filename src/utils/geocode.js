const request = require('request');
const ACCESS_TOKKEN =
    'pk.eyJ1IjoiY29nbmVzIiwiYSI6ImNrNXowaGt2azBhODIzZW1sOHh4bnV1bXgifQ.JIjNlzmAOXcQ6HU4cT-KhQ';
const geocode = (address, callback) => {
    const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${address}.json?access_token=${ACCESS_TOKKEN}`;
    request({ url, json: true }, (err, { body } = {}) => {
        if (err) {
            callback('Unable to connect to mapbox service!', undefined);
        } else if (body.features.length === 0) {
            callback('Unable to find place. Try another search.', undefined);
        } else {
            const features = body.features[0];
            const latitude = features.center[0];
            const longitude = features.center[1];
            callback(undefined, {
                location: features.place_name,
                latitude,
                longitude
            });
        }
    });
};

module.exports = geocode;
