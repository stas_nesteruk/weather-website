const chalk = require('chalk');

module.exports = {
    info: (...args) => console.log(chalk.bold.green(...args)),
    infoBGGreen: (...args) => console.log(chalk.bold.green.inverse(...args)),
    debug: (...args) => {
        args.forEach(elem => {
            if (Object.keys(elem).length > 0) {
                console.log(chalk.bold.cyan(JSON.stringify(elem)));
            } else {
                console.log(chalk.bold.cyan(elem));
            }
        });
    },
    warning: (...args) => console.log(chalk.bold.yellow(...args)),
    error: (...args) => console.log(chalk.bold.red(...args))
};
